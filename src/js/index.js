function init() {

    var canvas = document.getElementById('my-canvas');
    
    if (canvas.getContext) { // Checking for support

        var ctx = canvas.getContext('2d');



        // Canvas size
        function setCanvasSize() {
            var myBlockWidth = document.getElementsByClassName('my-block')[0].clientWidth;
            var myBlockHeight = document.getElementsByClassName('my-block')[0].clientHeight;
    
            ctx.canvas.width  = myBlockWidth;
            ctx.canvas.height = myBlockHeight;
        }
        setCanvasSize();



        // Canvas resize
        window.onresize = function(event) {
            setCanvasSize();
            coverImg(backgroundImage);
            drawBase();
        };



        // Set background image
        var backgroundImage = new Image();
        backgroundImage.src = '/img/sea.jpg';

        backgroundImage.onload = function() {
            coverImg(backgroundImage);
        }

        var coverImg = function coverImg(backgroundImage) {
            var myBlockWidth = document.getElementsByClassName('my-block')[0].clientWidth;
            var myBlockHeight = document.getElementsByClassName('my-block')[0].clientHeight;
            
            var imgRatio = backgroundImage.height / backgroundImage.width;
            var canvasRatio = myBlockHeight / myBlockWidth;
          
            if (imgRatio > canvasRatio) {
              var h = myBlockWidth * imgRatio;
              ctx.drawImage(backgroundImage, 0, (myBlockHeight - h) / 2, myBlockWidth, h);
            }
          
            if (imgRatio < canvasRatio) {
              var w = myBlockWidth * canvasRatio / imgRatio;
              ctx.drawImage(backgroundImage, (myBlockWidth - w) / 2, 0, w, myBlockHeight);
            }
        };



        // Create base
        var baseImage = new Image();
        baseImage.src = '/img/base.png';

        baseImage.onload = function() {
            drawBase();
        }

        function drawBase() {
            var myBlockWidth = document.getElementsByClassName('my-block')[0].clientWidth;
            var myBlockHeight = document.getElementsByClassName('my-block')[0].clientHeight;

            ctx.drawImage(baseImage, (myBlockWidth / 2) - (baseImage.width / 2), (myBlockHeight / 2) - (baseImage.height / 2));
        }

        
    }
}

document.addEventListener('DOMContentLoaded', init);